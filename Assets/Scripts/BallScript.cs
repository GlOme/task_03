﻿using Mirror;
using UnityEngine;

public class BallScript : NetworkBehaviour
{
    public int lastPlayer;
    public void Start()
    {
        if (isClientOnly)
        {
            GetComponent<Collider>().enabled = false;
            GetComponent<Rigidbody>().isKinematic = true;
        }
    }
    void OnCollisionEnter(Collision myCollision)
    {
        if (myCollision.gameObject.tag == "Player")
        {
            lastPlayer = myCollision.gameObject.GetComponent<MyCharacterController>().Name;
        }
    }
}
