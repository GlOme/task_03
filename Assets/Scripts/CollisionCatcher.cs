﻿using System;
using UnityEngine;

public enum Team
{
    Red,
    Green
}
public class CollisionCatcher : MonoBehaviour
{
    private const string ballTag = "Ball";

    public Team MyTeam;
    public Action<GameObject> OnGoal;    
    public int player;

    private void OnTriggerEnter(Collider myTrigger)
    {        
        if (myTrigger.gameObject.transform.CompareTag(ballTag))
        {
            player = myTrigger.GetComponent<BallScript>().lastPlayer;
            OnGoal?.Invoke(gameObject);
        }
    }
}
