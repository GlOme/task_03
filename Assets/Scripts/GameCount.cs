﻿using Mirror;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class GameCount : NetworkBehaviour
{
    public GameObject Ball;

    [SerializeField] 
    private Text RedGoalsText;
    [SerializeField]
    private Text GreenGoalsText;
    [SerializeField]
    private Text LogGoalText;
    [SerializeField]
    private CollisionCatcher RedCatcher;
    [SerializeField]
    private CollisionCatcher GreenCatcher;

    private int RedGoals;
    private int GreenGoals;    
    
    private int player;

    void Start()
    {
        if (NetworkServer.active)
        {
            RedCatcher.OnGoal += OnGoal;
            GreenCatcher.OnGoal += OnGoal;
        }
    }
    private void OnGoal(GameObject go)
    {
        CollisionCatcher tmp= go.GetComponent<CollisionCatcher>();
        if (tmp)
        {
            if (tmp.MyTeam == Team.Red)
            {
                GreenGoals++;
                GreenGoalsText.text = GreenGoals.ToString();
            }
            if (tmp.MyTeam == Team.Green)
            {
                RedGoals++;
                RedGoalsText.text = RedGoals.ToString();
            }
            player = tmp.player;
            RpcGetGoals(RedGoals, GreenGoals, player);
            Invoke("ResetClientBall", 2.0f);
        }
    }
    private void ResetClientBall()
    {
        if (Ball)
        {
            Ball.transform.position = new Vector3(-22, 1, 4);
            Ball.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
            Ball.GetComponent<Rigidbody>().angularVelocity = new Vector3(0, 0, 0);
        }
    }
    [ClientRpc]
    private void RpcGetGoals(int red, int green, int player)
    {
        GreenGoals=green;
        GreenGoalsText.text = GreenGoals.ToString();
        RedGoals = red;
        RedGoalsText.text = RedGoals.ToString();
        LogGoalText.text += new StringBuilder("Player_" + player + " have scored a goal; \r\n");
    }
    

}
