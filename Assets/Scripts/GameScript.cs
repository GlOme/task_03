﻿using Mirror;
using System.Collections.Generic;
using UnityEngine;

public class GameScript : NetworkManager
{
    [SerializeField]
    private int minPlayers;
    [SerializeField]
    private GameObject ballPrefab;
    [SerializeField]
    private List<GameObject> playersSpawn = new List<GameObject>();
    private int greenPlayers = 0, redPlayers = 0;
    private List<GameObject> players = new List<GameObject>();
    

    private GameObject ball;
    private bool IsInGameNow = false;
    private int playersCount=0;

    public override void OnServerAddPlayer(NetworkConnection conn)   
    {
        int team = addPlayerToTeam();
        Vector3 spawnPos;
        if (playersCount <= playersSpawn.Count)
        {
            spawnPos = playersSpawn[playersCount].transform.position;
        }
        else {
            Debug.Log("It is limit players on server");
            return;
        }
        GameObject go = Instantiate(playerPrefab, spawnPos, new Quaternion(0f,0f,0f,0f));
        playersCount++;
        go.GetComponent<MyCharacterController>().Name = playersCount;
        go.name = "Player_"+(playersCount).ToString();
        go.GetComponent<MyCharacterController>().team = team;
        NetworkServer.AddPlayerForConnection (conn, go);
       
        if (numPlayers >= minPlayers) StartPlay();
    }
    private int addPlayerToTeam()
    {
        if (greenPlayers <= redPlayers)
        {
            greenPlayers++;
            return 0;
        }
        else
        {
            redPlayers++;
            return 1;
        }
    }
    public override void OnServerDisconnect(NetworkConnection conn)
    {
        base.OnServerDisconnect(conn);
        removePlayerFromTeam(conn);
        if (numPlayers < minPlayers) StopPlay();
    }
    private void removePlayerFromTeam(NetworkConnection conn)
    {
        for(int i = 0; i < players.Count; i++)
        {
            if (conn.clientOwnedObjects.Contains( players[i].GetComponent<NetworkIdentity>()))
            {
                players.RemoveAt(i);
                return;
            }
        }
    }
    
    void StartPlay()
    {
        if (!IsInGameNow)
        {
            ball = Instantiate(ballPrefab);
            NetworkServer.Spawn(ball);
            IsInGameNow = true;
            FindObjectOfType<GameCount>().Ball=ball;
        }
    }
    void StopPlay()
    {
        if (IsInGameNow)
        {
            NetworkServer.Destroy(ball);
            IsInGameNow = false;
        }
    }
    
}
