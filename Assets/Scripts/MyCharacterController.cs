﻿using Mirror;
using System;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class MyCharacterController : NetworkBehaviour
{
    [SyncVar]
    public int team = 0;
    [SyncVar]
    private int _name;
    #region
    [SerializeField]
    private Rigidbody rb;
    [SerializeField]
    private Animator anim;
    [SerializeField]
    private CharacterSkinController skin;
    [SerializeField]
    private bl_Joystick joystick;
    [SerializeField]
    private Text IdText;
    [SerializeField]
    private Canvas canvas;

    [SerializeField]
    private float maxSpeed;
    [SerializeField]
    private float speedDumping;
    [SerializeField]
    private float rotationSpeed;
    [SerializeField]
    private float rotationDumping;

    [SerializeField]
    private float jumpForce;
    [SerializeField]
    private float jumpDump;
    [SerializeField]
    private Vector3 gravityVector;

    [SerializeField]
    private float moveToAnimForce;
    [SerializeField]
    private float moveToAnimDump;

    [SerializeField]
    private Camera myCam;

    private float blend;

    private Vector3 curRotVector;
    private Vector3 targRotVector;
    private float currSpeed;
    private float targSpeed;

    private Vector3 curJumpVector;

    private Vector3 prevPos;

    private bool isGrounded = true;
    private float ax1 = 0f, ax2 = 0f, ax3 = 0f;
    #endregion
    
    private void Start()
    {
        targRotVector=curRotVector = transform.rotation.eulerAngles;
        gameObject.name = "Player_"+_name;

        if (!hasAuthority)
        {
            rb.isKinematic = false;
            myCam.gameObject.SetActive(false);
            canvas.gameObject.SetActive(false);
        }
        IdText.text = GetComponent<NetworkIdentity>().netId.ToString();
        skin.ChangeMaterialSettings(team);
    }
    private void OnCollisionStay(Collision collision)
    {
        if (collision.transform.CompareTag("Ground")) isGrounded = true;
    }
    void Update()
    {
       if (hasAuthority & AxisesChenged()) UpdateMove();
        AnimationUpdate();
    }
    private void UpdateMove()
    {

        if (isClient)
        {
            CmdInput(Input.GetAxis("Vertical"), Input.GetAxis("Horizontal"), Input.GetAxis("Jump"));
        }
    }
    private bool AxisesChenged()
    {
        if (ax1 != Input.GetAxis("Vertical") || ax2 != Input.GetAxis("Horizontal") || ax3 != Input.GetAxis("Jump"))
        {
            //ax1 = Input.GetAxis("Vertical");
            //ax2 = Input.GetAxis("Horizontal");
            //ax3 = Input.GetAxis("Jump");
            return true; 
        }
        return false;
    }
    //split move
    [Command]
    private void CmdInput(float val1, float val2, float val3)
    {
        targSpeed = val1 * maxSpeed;
        currSpeed = Mathf.Lerp(currSpeed, targSpeed, Time.deltaTime * speedDumping);

        targRotVector += new Vector3(0, val2 * rotationSpeed, 0);

        curRotVector = Vector3.Lerp(curRotVector, targRotVector, Time.deltaTime * rotationDumping);
        transform.eulerAngles = curRotVector;
        if (isGrounded)
        {
            curJumpVector = Vector3.zero;
        }
        else
        {
            curJumpVector = Vector3.Lerp(curJumpVector, gravityVector, Time.deltaTime * jumpDump);
        }
        if (isGrounded && val3 > 0) curJumpVector = Vector3.up * jumpForce;

        rb.velocity = transform.forward * currSpeed + curJumpVector;
        isGrounded = false;
    }
    private void AnimationUpdate()
    {
        blend = Mathf.Lerp(blend, Vector3.Distance(transform.position, prevPos) * moveToAnimForce, Time.deltaTime * moveToAnimDump);
        anim.SetFloat("Blend", blend);
        prevPos = transform.position;
    }

    public int Name { get => _name; set => _name = value; }

}
